# go-build-example

The repository contains a sample of GoLang project configured to be built within Gitlab CI.

## Installation

Please install build tools by running:
```
$ go get -u github.com/magefile/mage
$ go get -u github.com/golang/dep/cmd/dep
$ go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
```

## Building

This command could be used to execute default build flow:
```
$ mage -v 
```
or 
```
$ mage -v build
```
