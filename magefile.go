// +build mage

package main

import (
	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

var Default = Build

// Fmt format the code with `go fmt` and `goimports`.
func Fmt() error {
	err := sh.Run("gofmt", "-s", "-w", "./")
	if err != nil {
		return err
	}
	return sh.Run("goimports", "-l", "-w", "./")
}

// Dep install application dependencies.
func Dep() error {
	return sh.Run("dep", "ensure")
}

// Test test the application.
func Test() error {
	return sh.Run("go", "test", "./...")
}

// Lint lint the application.
func Lint() error {
	return sh.Run("golangci-lint", "run", "--enable-all", "--tests=false", "./...")
}

// Compile compile the application.
func Compile() error {
	return sh.Run("go", "build")
}

// Build build the application.
func Build() {
	mg.SerialDeps(Fmt, Dep, Test, Lint, Compile)
}
